---
title:    "Mesa 21.1.4 is released"
date:     2021-06-30
category: releases
tags:     []
---
[Mesa 21.1.4](https://docs.mesa3d.org/relnotes/21.1.4.html) is released. This is a bug fix release.
