---
title:    "Mesa 19.2.1 is released"
date:     2019-10-09
category: releases
tags:     []
---
[Mesa 19.2.1](https://docs.mesa3d.org/relnotes/19.2.1.html) is released. This is a bug fix
release.
