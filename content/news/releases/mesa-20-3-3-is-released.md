---
title:    "Mesa 20.3.3 is released"
date:     2021-01-13
category: releases
tags:     []
---
[Mesa 20.3.3](https://docs.mesa3d.org/relnotes/20.3.3.html) is released. This is a bug fix release.
