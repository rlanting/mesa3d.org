---
title:    "Mesa 19.0.8 is released"
date:     2019-06-26
category: releases
tags:     []
---
[Mesa 19.0.8](https://docs.mesa3d.org/relnotes/19.0.8.html) is released. This is an emergency
bug fix release. Users of 19.0.7 should updated to 19.0.8 or 19.1.1
immediately.
