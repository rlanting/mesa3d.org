---
title:    "Mesa 10.3.4 is released"
date:     2014-11-21
category: releases
tags:     []
---
[Mesa 10.3.4](https://docs.mesa3d.org/relnotes/10.3.4.html) is released. This is a bug-fix
release.
