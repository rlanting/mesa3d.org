---
title:    "Mesa 10.0.1 and Mesa 9.2.5 are released"
date:     2013-12-12
category: releases
tags:     []
---
[Mesa 10.0.1](https://docs.mesa3d.org/relnotes/10.0.1.html) and [Mesa
9.2.5](https://docs.mesa3d.org/relnotes/9.2.5.html) are released. These are both bug-fix
releases.
