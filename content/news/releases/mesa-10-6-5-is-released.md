---
title:    "Mesa 10.6.5 is released"
date:     2015-08-22
category: releases
tags:     []
---
[Mesa 10.6.5](https://docs.mesa3d.org/relnotes/10.6.5.html) is released. This is a bug-fix
release.
