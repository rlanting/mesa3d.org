---
title:    "Mesa 17.1.8 is released"
date:     2017-08-28
category: releases
tags:     []
---
[Mesa 17.1.8](https://docs.mesa3d.org/relnotes/17.1.8.html) is released. This is a bug-fix
release.
