---
title:    "Mesa 17.3.4 is released"
date:     2018-02-15
category: releases
tags:     []
---
[Mesa 17.3.4](https://docs.mesa3d.org/relnotes/17.3.4.html) is released. This is a bug-fix
release.
