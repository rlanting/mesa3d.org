---
title:    "Mesa 18.3.3 is released"
date:     2019-01-31
category: releases
tags:     []
---
[Mesa 18.3.3](https://docs.mesa3d.org/relnotes/18.3.3.html) is released. This is a bug-fix
release.
