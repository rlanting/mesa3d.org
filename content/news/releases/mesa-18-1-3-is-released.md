---
title:    "Mesa 18.1.3 is released"
date:     2018-06-29
category: releases
tags:     []
---
[Mesa 18.1.3](https://docs.mesa3d.org/relnotes/18.1.3.html) is released. This is a bug-fix
release.
