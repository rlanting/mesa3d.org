---
title:    "Mesa 19.1.6 is released"
date:     2019-09-03
category: releases
tags:     []
---
[Mesa 19.1.6](https://docs.mesa3d.org/relnotes/19.1.6.html) is released. This is a bug-fix
release.
