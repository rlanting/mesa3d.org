---
title:    "Mesa 21.1.1 is released"
date:     2021-05-19
category: releases
tags:     []
---
[Mesa 21.1.1](https://docs.mesa3d.org/relnotes/21.1.1.html) is released. This is a bug fix release.
