---
title:    "Mesa 21.3.7 is released"
date:     2022-02-23
category: releases
tags:     []
---
[Mesa 21.3.7](https://docs.mesa3d.org/relnotes/21.3.7.html) is released. This is a bug fix release.
