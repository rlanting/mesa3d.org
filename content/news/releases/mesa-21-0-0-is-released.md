---
title:    "Mesa 21.0.0 is released"
date:     2021-03-11
category: releases
tags:     []
aliases:  [/news/releases/mesa-21-0-is-released/]
---
[Mesa 21.0.0](https://docs.mesa3d.org/relnotes/21.0.0.html) is released. This is a new development release. See the release notes for more information about this release.
