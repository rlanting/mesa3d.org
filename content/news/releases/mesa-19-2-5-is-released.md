---
title:    "Mesa 19.2.5 is released"
date:     2019-11-20
category: releases
tags:     []
---
[Mesa 19.2.5](https://docs.mesa3d.org/relnotes/19.2.5.html) is released. This is a bug fix
release.
