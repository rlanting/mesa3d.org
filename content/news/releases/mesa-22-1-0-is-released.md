---
title:    "Mesa 22.1.0 is released"
date:     2022-05-18
category: releases
tags:     []
---
[Mesa 22.1.0](https://docs.mesa3d.org/relnotes/22.1.0.html) is released. This is a new development release. See the release notes for more information about this release.
