---
title:    "Mesa 18.1.6 is released"
date:     2018-08-13
category: releases
tags:     []
---
[Mesa 18.1.6](https://docs.mesa3d.org/relnotes/18.1.6.html) is released. This is a bug-fix
release.
