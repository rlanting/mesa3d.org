---
title:    "Mesa 20.0.7 is released"
date:     2020-05-14
category: releases
tags:     []
---
[Mesa 20.0.7](https://docs.mesa3d.org/relnotes/20.0.7.html) is released. This is a bug fix
release.
