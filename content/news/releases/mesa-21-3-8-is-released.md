---
title:    "Mesa 21.3.8 is released"
date:     2022-03-18
category: releases
tags:     []
---
[Mesa 21.3.8](https://docs.mesa3d.org/relnotes/21.3.8.html) is released. This is a bug fix release.
