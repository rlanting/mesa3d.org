---
title:    "Mesa 21.3.1 is released"
date:     2021-12-01
category: releases
tags:     []
---
[Mesa 21.3.1](https://docs.mesa3d.org/relnotes/21.3.1.html) is released. This is a bug fix release.
