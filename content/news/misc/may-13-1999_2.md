---
title:    "May 13, 1999"
date:     1999-05-13 00:00:00
category: misc
tags:     []
---
    May 1999 - John Carmack of id Software, Inc. has made a donation of
    US$10,000 to the Mesa project to support its continuing development.
    Mesa is a free implementation of the OpenGL 3D graphics library and id's
    newest game, Quake 3 Arena, will use Mesa as the 3D renderer on Linux.
    
    The donation will go to Keith Whitwell, who has been optimizing Mesa to
    improve performance on 3d hardware.  Thanks to Keith's work, many
    applications using Mesa 3.1 will see a dramatic performance increase
    over Mesa 3.0.  The donation will allow Keith to continue working on
    Mesa full time for some time to come.
    
    For more information about Mesa see www.mesa3d.org.  For more
    information about id Software, Inc. see www.idsoftware.com.
    
    --------------------------------
    
    This donation from John/id is very generous.  Keith and I are very
    grateful.
