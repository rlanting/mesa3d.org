---
title:    "March 26, 2007"
date:     2007-03-26 00:00:00
category: misc
tags:     []
---
The new Shading Language compiler branch has been merged into the git
master branch. This is a step toward hardware support for the OpenGL 2.0
Shading Language and will be included in the next Mesa release. In
conjunction, [Glean](http://glean.sf.net) has been updated with a new
test that does over 130 tests of the shading language and built-in
functions.
