---
title:    "September 21, 1999"
date:     1999-09-21 00:00:00
category: misc
tags:     []
---
There appear to be two new files on the ftp site,
`MesaLib-3.1beta3.tar.gz` and `MesaDemos-3.1beta3.tar.gz`, that seem to
be... yes, I've just received confirmation from the beta center, they
are indeed the **THIRD** beta release of Mesa 3.1\! Happy Days. Happy
Days. Thanks Keith Whitwell for preparing these for us during Brian's
absence.
